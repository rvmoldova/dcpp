std::vector<Planeta *> listaPlanete;
std::vector<Savant *> listaSavanti;

void afisareMeniu()
{
    std::cout << "1. Adauga Planeta\n";
    std::cout << "2. Adauga Savant\n";
    std::cout << "3. Sortare Planete\n";
    std::cout << "4. Sortare Savanti\n";
    std::cout << "5. Afisare Planete\n";
    std::cout << "6. Afisare Savanti\n";
    std::cout << "7. Afisare toate planetele si de cine a fost descoperita\n";
    std::cout << "8. Iesire\n";
}

void initializare()
{
    Savant *savantNou1 = new Savant();

    savantNou1
        ->setNume("Doina")
        ->setPrenume("Cirlig")
        ->setTara("Moldova")
        ->setAnulNasterii(1900);

    listaSavanti.push_back(savantNou1);

    Planeta *planetaNoua1 = new Planeta();
    planetaNoua1
        ->setDenumire("Pamant")
        ->setPerioadaDeRotatie(365)
        ->setDistantaLaSoare(149600000)
        ->setDescoperitor(savantNou1);

    listaPlanete.push_back(planetaNoua1);

    Savant *savantNou2 = new Savant();

    savantNou2
        ->setNume("John")
        ->setPrenume("Smidth")
        ->setTara("SUA")
        ->setAnulNasterii(1945);

    listaSavanti.push_back(savantNou2);

    Planeta *planetaNoua2 = new Planeta();
    planetaNoua2
        ->setDenumire("Jupiter")
        ->setPerioadaDeRotatie(1350)
        ->setDistantaLaSoare(349600000)
        ->setDescoperitor(savantNou2);

    listaPlanete.push_back(planetaNoua2);
}

Savant *adaugaSavant()
{
    std::string numeSavant, prenumeSavant, taraSavant;
    int anulNasterii;

    std::cout << "Introdu Numele Savantului\n";
    std::cin >> numeSavant;
    std::cout << "Introdu Prenumele Savantului\n";
    std::cin >> prenumeSavant;
    std::cout << "Introdu Tara Savantului\n";
    std::cin >> taraSavant;
    std::cout << "Introdu Anul Nasterii a Savantului\n";
    std::cin >> anulNasterii;

    Savant *savantNou = new Savant();

    savantNou
        ->setNume(numeSavant)
        ->setPrenume(prenumeSavant)
        ->setTara(taraSavant)
        ->setAnulNasterii(anulNasterii);

    listaSavanti.push_back(savantNou);

    return savantNou;
}

Planeta *adaugaPlaneta()
{
    std::string numePlaneta;
    int perioadaDeRotatie;
    long long distantaLaSoare;

    std::cout << "Introdu Denumirea Planetei\n";
    std::cin >> numePlaneta;
    std::cout << "Introdu Perioada de rotatie a Planetei\n";
    std::cin >> perioadaDeRotatie;
    std::cout << "Introdu distanta de la soare a Planetei\n";
    std::cin >> distantaLaSoare;

    Planeta *planetaNoua = new Planeta();

    Savant *savantNou = adaugaSavant();

    planetaNoua
        ->setDenumire(numePlaneta)
        ->setPerioadaDeRotatie(perioadaDeRotatie)
        ->setDistantaLaSoare(distantaLaSoare)
        ->setDescoperitor(savantNou);

    listaPlanete.push_back(planetaNoua);

    std::cout << listaPlanete.size() << "\n";
    return planetaNoua;
}

void afisarePlanete()
{
    Planeta::arataHeader();
    for (std::vector<Planeta *>::iterator it = listaPlanete.begin(); it != listaPlanete.end(); it++)
    {
        (*it)->afisare();
    }
}

void afisareSavanti()
{
    Savant::arataHeader();
    for (std::vector<Savant *>::iterator it = listaSavanti.begin(); it != listaSavanti.end(); it++)
    {
        (*it)->afisare();
    }
}

void afisareInfo()
{
    for (std::vector<Planeta *>::iterator it = listaPlanete.begin(); it != listaPlanete.end(); it++)
    {
        (*it)->info();
    }
}

void sortPlanete()
{
    std::cout << "Sortare planete dupa\n";
    std::cout << "1. Denumire\n";
    std::cout << "2. Distanta de la Soare\n";
    std::cout << "3. Perioada de rotatie\n";
    bool valid = true;
    while (valid)
    {
        int optiune;
        std::cin >> optiune;
        switch (optiune)
        {
        case 1:
            std::sort(listaPlanete.begin(), listaPlanete.end(), sortPlanetaNume);
            valid = false;
            break;
        case 2:
            std::sort(listaPlanete.begin(), listaPlanete.end(), sortPlanetaDistanta);
            valid = false;
            break;
        case 3:
            std::sort(listaPlanete.begin(), listaPlanete.end(), sortPlanetaPerRot);
            valid = false;
            break;
        default:
            std::cout << "Optiune inexistenta\n";
            break;
        }
    }
}

void sortSavanti()
{
    std::cout << "Sortare savanti dupa\n";
    std::cout << "1. Nume\n";
    std::cout << "2. Prenume\n";
    std::cout << "3. Anul Nasterii\n";
    std::cout << "4. Tara\n";
    bool valid = true;
    while (valid)
    {
        int optiune;
        std::cin >> optiune;
        switch (optiune)
        {
        case 1:
            std::sort(listaSavanti.begin(), listaSavanti.end(), sortSavantNume);
            valid = false;
            break;
        case 2:
            std::sort(listaSavanti.begin(), listaSavanti.end(), sortSavantPrenume);
            valid = false;
            break;
        case 3:
            std::sort(listaSavanti.begin(), listaSavanti.end(), sortSavantAnulNasterii);
            valid = false;
            break;
        case 4:
            std::sort(listaSavanti.begin(), listaSavanti.end(), sortSavantTara);
            valid = false;
            break;
        default:
            std::cout << "Optiune inexistenta\n";
            break;
        }
    }
}

/**
 * iesire
 * inainte de terminarea programului curatim memoria alocata pentru lista de planete si savanti
 */
void iesire()
{
    while (!listaPlanete.empty())
    {
        delete listaPlanete.back();
        listaPlanete.pop_back();
    }
    while (!listaSavanti.empty())
    {
        delete listaSavanti.back();
        listaSavanti.pop_back();
    }
}
