class Planeta
{
  private:
    std::string denumire;
    long long distantaLaSoare;
    int perioadaDeRotatie;
    Savant *descoperitor;

  public:
    static void arataHeader();
    Planeta();
    Planeta(std::string, long long, int);
    std::string getDenumire();
    long long getDistantaLaSoare();
    int getPerioadaDeRotatie();
    Savant *getDescoperitor();
    Planeta *setDenumire(std::string);
    Planeta *setDistantaLaSoare(long long);
    Planeta *setPerioadaDeRotatie(int);
    Planeta *setDescoperitor(Savant *);
    void afisare();
    void info();
};

Planeta::Planeta()
{
    this->denumire = "Planeta noua";
    this->distantaLaSoare = 1496000000;
    this->perioadaDeRotatie = 365;
}

Planeta::Planeta(std::string denumire, long long distantaLaSoare, int perioadaDeRotatie)
{
    this->denumire = denumire;
    this->distantaLaSoare = distantaLaSoare;
    this->perioadaDeRotatie = perioadaDeRotatie;
}

std::string Planeta::getDenumire()
{
    return this->denumire;
}

long long Planeta::getDistantaLaSoare()
{
    return this->distantaLaSoare;
}

int Planeta::getPerioadaDeRotatie()
{
    return this->perioadaDeRotatie;
}

Savant *Planeta::getDescoperitor()
{
    return this->descoperitor;
}

Planeta *Planeta::setDenumire(std::string denumire)
{
    this->denumire = denumire;
    return this;
}

Planeta *Planeta::setDistantaLaSoare(long long distantaLaSoare)
{
    this->distantaLaSoare = distantaLaSoare;
    return this;
}

Planeta *Planeta::setPerioadaDeRotatie(int perioadaDeRotatie)
{
    this->perioadaDeRotatie = perioadaDeRotatie;
    return this;
}

Planeta *Planeta::setDescoperitor(Savant *descoperitor)
{
    this->descoperitor = descoperitor;
    return this;
}

void Planeta::arataHeader()
{
    std::cout << std::setw(15) << "denumire"
              << " | ";
    std::cout << std::setw(15) << "distantaLaSoare"
              << " | ";
    std::cout << std::setw(15) << "perioadaDeRotatie"
              << "\n";
}

void Planeta::afisare()
{
    std::cout << std::setw(15) << this->getDenumire() << " | ";
    std::cout << std::setw(15) << this->getDistantaLaSoare() << " | ";
    std::cout << std::setw(15) << this->getPerioadaDeRotatie() << "\n";
}

void Planeta::info()
{
    std::cout << "Planeta " << this->getDenumire() << " a fost descoperita de " << this->getDescoperitor()->getNume() << " " << this->getDescoperitor()->getPrenume() << "\n";
}