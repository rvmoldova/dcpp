bool sortPlanetaNume(Planeta *a, Planeta *b)
{
    return a->getDenumire() < b->getDenumire();
}

bool sortPlanetaDistanta(Planeta *a, Planeta *b)
{
    return a->getDistantaLaSoare() < b->getDistantaLaSoare();
}

bool sortPlanetaPerRot(Planeta *a, Planeta *b)
{
    return a->getPerioadaDeRotatie() < b->getPerioadaDeRotatie();
}

bool sortSavantNume(Savant *a, Savant *b)
{
    return a->getNume() < b->getNume();
}

bool sortSavantPrenume(Savant *a, Savant *b)
{
    return a->getPrenume() < b->getPrenume();
}

bool sortSavantTara(Savant *a, Savant *b)
{
    return a->getTara() < b->getTara();
}

bool sortSavantAnulNasterii(Savant *a, Savant *b)
{
    return a->getAnulNasterii() < b->getAnulNasterii();
}