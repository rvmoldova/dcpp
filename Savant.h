class Savant
{
  private:
    std::string nume;
    std::string prenume;
    std::string tara;
    int anulNasterii;

  public:
    Savant();
    Savant(std::string, std::string, std::string, int);
    std::string getNume();
    std::string getPrenume();
    std::string getTara();
    int getAnulNasterii();
    Savant *setNume(std::string);
    Savant *setPrenume(std::string);
    Savant *setTara(std::string);
    Savant *setAnulNasterii(int);
    static void arataHeader();
    void afisare();
};

Savant::Savant()
{
    this->nume = "Savant";
    this->prenume = "Nou";
    this->tara = "Moldova";
    this->anulNasterii = 1970;
}

Savant::Savant(std::string nume, std::string prenume, std::string tara, int anulNasterii)
{
    this->nume = nume;
    this->prenume = prenume;
    this->tara = tara;
    this->anulNasterii = anulNasterii;
}

std::string Savant::getNume()
{
    return this->nume;
}

std::string Savant::getPrenume()
{
    return this->prenume;
}

std::string Savant::getTara()
{
    return this->tara;
}

int Savant::getAnulNasterii()
{
    return this->anulNasterii;
}

Savant *Savant::setNume(std::string nume)
{
    this->nume = nume;
    return this;
}

Savant *Savant::setPrenume(std::string prenume)
{
    this->prenume = prenume;
    return this;
}

Savant *Savant::setTara(std::string tara)
{
    this->tara = tara;
    return this;
}

Savant *Savant::setAnulNasterii(int anulNasterii)
{
    this->anulNasterii = anulNasterii;
    return this;
}

void Savant::arataHeader()
{
    std::cout << std::setw(15) << "Nume"
              << " | ";
    std::cout << std::setw(15) << "Prenume"
              << " | ";
    std::cout << std::setw(15) << "Tara"
              << " | ";
    std::cout << std::setw(15) << "Anul Nasterii"
              << "\n";
}

void Savant::afisare()
{
    std::cout << std::setw(15) << this->getNume() << " | ";
    std::cout << std::setw(15) << this->getPrenume() << " | ";
    std::cout << std::setw(15) << this->getTara() << " | ";
    std::cout << std::setw(15) << this->getAnulNasterii() << "\n";
}
