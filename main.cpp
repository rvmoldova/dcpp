#include <algorithm>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

#include "Savant.h"
#include "Planeta.h"
#include "functiiSortare.h"
#include "meniu.h"

int main()
{
    bool valid = true;
    initializare();
    while (valid)
    {
        afisareMeniu();
        int optiune;
        std::cin >> optiune;
        switch (optiune)
        {
        case 1:
            adaugaPlaneta();
            break;
        case 2:
            adaugaSavant();
            break;
        case 3:
            sortPlanete();
            break;
        case 4:
            sortSavanti();
            break;
        case 5:
            afisarePlanete();
            break;
        case 6:
            afisareSavanti();
            break;
        case 7:
            afisareInfo();
            break;
        case 8:
            valid = false;
            iesire();
            break;
        default:
            std::cout << "Optiune inexistenta\n";
            break;
        }
    }
    return 0;
}
